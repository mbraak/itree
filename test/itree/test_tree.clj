(ns itree.test-tree
  (:require
    [clojure.test :refer :all]
    [itree.cljc.tree :as tree]
    [itree.cljc.node :as node]
    [clojure.pprint :refer [pprint]]
    [clojure.spec.test :as stest]
  )
)

(stest/instrument)

(def data1
  [
    {
      :name "n1" :id 1
      :children [
        {:name "n1a" :id 2}
        {:name "n1b" :id 3}
      ]
    }
    {
      :name "n2" :id 4
      :children [
        {:name "n2a" :id 5}
        {:name "n2b" :id 6}
      ]
    }
  ]
)

(defn get-node-names [nodes]
  (map
    (fn [n]
      (get n :name)
    )
    nodes
  )
)

(deftest test-tree
  (testing "create empty tree"
    (let
      [
        t (tree/create)
      ]
      (is
        (=
          ""
          (tree/to-string t)
        )
      )

      (is
        (=
          0
          (count (tree/get-children t))
        )
      )
    )
  )

  (testing "create tree from data"
    (let
      [
        t (tree/create data1)
      ]

      (is
        (=
          2
          (count (tree/get-children t))
        )
      )

      (is
        (=
          "n1(n1a n1b) n2(n2a n2b)"
          (tree/to-string t)
        )
      )
    )
  )

  (testing "add-node"
    (let
      [
        t1 (tree/create)
        t2 (tree/add-node t1 {:name "n1" :id 1})
      ]
      (is
        (=
          ""
          (tree/to-string t1)
        )
      )

      (is
        (=
          "n1"
          (tree/to-string t2)
        )
      )
    )
  )

  (testing "get-node-by-name"
    (let
      [
        t (tree/create data1)

        n2a (tree/get-node-by-name t "n2a")
      ]
      (is
        (=
          "n2a"
          (:name n2a)
        )
      )

      (is
        (=
          5
          (:id n2a)
        )
      )
    )
  )

  (testing "add child node"
    (let
      [
        t1
        (->
          (tree/create)
          (tree/add-node {:name "n1" :id 1})
          (tree/add-node {:name "n2" :id 2})
        )

        n1 (tree/get-node-by-name t1 "n1")

        t2 (tree/add-node t1 n1 {:name "n1a" :id 3})
      ]
      (is
        (=
          "n1 n2"
          (tree/to-string t1)
        )
      )
      (is
        (=
          "n1(n1a) n2"
          (tree/to-string t2)
        )
      )
    )
  )

  (testing "remove-node"
    (let
      [
        t1
        (->
          (tree/create)
          (tree/add-node {:name "n1" :id 1})
          (tree/add-node {:name "n2" :id 2})
        )

        n1 (tree/get-node-by-name t1 "n1")

        t2 (tree/remove-node t1 n1)
      ]
      (is
        (=
          "n1 n2"
          (tree/to-string t1)
        )
      )

      (is
        (=
          "n2"
          (tree/to-string t2)
        )
      )

      (is
        (=
          nil
          (tree/get-node-by-id t2 1)
        )
      )
    )
  )

  (testing "remove node with children"
    (let
      [
        t1 (tree/create data1)
        n2 (tree/get-node-by-name t1 "n2")
        t2 (tree/remove-node t1 n2)
      ]
      (is
        (=
          "n1(n1a n1b)"
          (tree/to-string t2)
        )
      )

      (is
        (=
          nil
          (tree/get-node-by-id t2 4)
        )
      )

      (is
        (=
          nil
          (tree/get-node-by-id t2 5)
        )
      )

    )
  )

  (testing "remove child node"
    (let
      [
        t1 (tree/create data1)
        n1a (tree/get-node-by-name t1 "n1a")
        t2 (tree/remove-node t1 n1a)
      ]
      (is
        (=
          "n1(n1a n1b) n2(n2a n2b)"
          (tree/to-string t1)
        )
      )
      (is
        (=
          "n1(n1b) n2(n2a n2b)"
          (tree/to-string t2)
        )
      )

      (is
        (=
          nil
          (tree/get-node-by-id t2 2)
        )
      )
    )
  )

  (testing "has-children?"
    (let
      [
        t1 (tree/create data1)
      ]
      (is (tree/has-children? t1))
    )
  )

  (testing "is-node-open"
    (let
      [
        t1 (tree/create data1)
        t2 (tree/open-node t1 1)
        t3 (tree/close-node t2 1)
        t4 (tree/toggle-node t3 1)
      ]
      (is (not (tree/is-node-open t1 1)))
      (is (tree/is-node-open t2 1))
      (is (not (tree/is-node-open t3 1)))
      (is (tree/is-node-open t4 1))
    )
  )

  (testing "get-node-by-id"
    (let
      [
        t1 (tree/create data1)
        t2 (tree/add-node t1 {:name "n3" :id 7})

        n2a (tree/get-node-by-name t1 "n2a")
        t3 (tree/remove-node t1 n2a)
      ]

      (is
        (=
          "n1a"
          (:name (tree/get-node-by-id t1 2))
        )
      )
      (is
        (=
          "n3"
          (:name (tree/get-node-by-id t2 7))
        )
      )

      (is
        (=
          nil
          (tree/get-node-by-id t3 5)
        )
      )
    )
  )

  (testing "select-node"
    (let
      [
        t1 (tree/create data1)
        t2 (tree/select-node t1 5)
        t3 (tree/select-node t2 6)
      ]
      (is (not (:is-selected (tree/get-node-by-id t1 5))))
      (is (:is-selected (tree/get-node-by-id t2 5)))
      (is (not (:is-selected (tree/get-node-by-id t3 5))))
      (is (:is-selected (tree/get-node-by-id t3 6)))
    )
  )

  (testing "get-parents"
    (let
      [
        t1 (tree/create data1)
        n1a (tree/get-node-by-name t1 "n1a")

        t2 (tree/add-node t1 n1a {:name "n1a1" :id 7})

        n1a1 (tree/get-node-by-name t2 "n1a1")
        parents (tree/get-parents t2 n1a1)
      ]
      (is
        (=
          "n1a n1 [root]"
          (node/node-list-to-string parents)
        )
      )
    )
  )
)
