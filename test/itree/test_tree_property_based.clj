(ns itree.test-tree-property-based
  (:require
    [clojure.test :refer :all]
    [clojure.test.check.generators :as gen]
    [stateful-check.core :refer :all]
    [itree.cljc.tree :as ptree]
    [clojure.spec.test :as stest]
  )
)

(stest/instrument)

(def trees-in-use (atom 0))

(defn new-tree []
  (atom (ptree/create))
)

(defn choose-node [tree i1]
  (let
    [
      ids (keys (:ids tree))
      id-choices (apply vector (conj ids 0))
      n-id-choices (count id-choices)
      parent-index (int (/ (* i1 n-id-choices) 10000))
      parent-id (get id-choices parent-index)
      root? (= parent-id 0)
    ]
    (if root?
      nil
      (ptree/get-node-by-id tree parent-id)
    )
  )
)

(defn add-node [tree-atom node-id name i1]
  (let
    [
      tree @tree-atom
      parent (choose-node tree i1)
      child {:name name :id node-id}

      new-tree
      (if (nil? parent)
        (ptree/add-node tree child)
        (ptree/add-node tree parent child)
      )
    ]
    (reset! tree-atom new-tree)
    nil
  )
)

(defn remove-node [tree-atom i1]
  (let
    [
      tree @tree-atom
      node (choose-node tree i1)

      new-tree
      (if (nil? node)
        nil
        (ptree/remove-node tree node)
      )
    ]
    nil
  )
)

(defn update-node [tree-atom i1 name]
  (let
    [
      tree @tree-atom
      node (choose-node tree i1)

      new-tree
      (if (nil? node)
        nil
        (ptree/update-node tree node {:name name})
      )
    ]
    nil
  )
)

(def add-command
  {
    :model/args
    (fn [state]
      [(:tree state) gen/uuid gen/string-alphanumeric (gen/choose 0 9999)]
    )

    :real/command #'add-node
  }
)

(def update-command
  {
    :model/args
    (fn [state]
      [(:tree state) (gen/choose 0 9999) gen/string-alphanumeric]
    )

    :real/command #'update-node
  }
)

(def remove-command
  {
    :model/args
    (fn [state]
      [(:tree state) (gen/choose 0 9999)]
    )

    :real/command #'remove-node
  }
)

(def tree-specification
  {:commands
    {
      :add #'add-command
      :remove #'remove-command
      :update #'update-command
    }

    :initial-state
    (fn [tree]
      {
        :tree tree
      }
    )

    :real/setup
    (fn []
      (swap! trees-in-use inc)
      (new-tree)
    )

    :real/cleanup
    (fn [state]
      (swap! trees-in-use dec)
    )
  }
)

(deftest tree-test
  (let
    [val @trees-in-use]
    (is (specification-correct? tree-specification))
    (is
      (= val @trees-in-use)
      "setup/cleanup should both be run for all tests (pass and fail)"
    )
  )
)
