(ns itree.test-node
  (:require
    [clojure.test :refer :all]
    [itree.cljc.node :as node]
    [clojure.pprint :refer [pprint]]
    [clojure.spec.test :as stest]
  )
)

(stest/instrument)

(def data1
  [
    {
      :name "n1" :id 1
      :children [
        {:name "n1a" :id 2}
        {:name "n1b" :id 3}
      ]
    }
    {
      :name "n2" :id 4
      :children [
        {:name "n2a" :id 5}
        {:name "n2b" :id 6}
      ]
    }
  ]
)

(deftest test-node
  (testing "has-children?"
    (let
      [
        t1 (node/create data1)
        n1 (:node (node/get-node-by-name t1 "n1"))
        n1a (:node (node/get-node-by-name t1 "n1a"))
      ]
      (is (node/has-children? n1))
      (is (not (node/has-children? n1a)))
    )
  )

  (testing "add-node"
    (let
      [
        t1 (node/create)
        [t2 changed-t2] (node/add-node t1 {:name "n1" :id 1})

        n1 (node/get-node-by-name t2 "n1")

        [t3 changed-t3]
        (node/add-node t2 n1 {:name "n1a" :id 2})

        n1a (node/get-node-by-name t3 "n1a")
        [t4 changed-t4]
        (node/add-node t3 n1a {:name "n1b" :id 3})
      ]
      (is
        (=
          "n1"
          (node/to-string t2)
        )
      )

      (is
        (=
          '(:new-child :changed-nodes)
          (keys changed-t2)
        )
      )

      (is
        (empty? (:changed-nodes changed-t2))
      )

      (is
        (=
          "n1(n1a)"
          (node/to-string t3)
        )
      )

      (is
        (:is-root t3)
      )

      (is
        (=
          "n1"
          (node/node-list-to-string (:changed-nodes changed-t3))
        )
      )

      (is
        (=
          {:id 2, :name "n1a", :parent-id 1}
          (:new-child changed-t3)
        )
      )

      (is
        (=
          1
          (get-in n1a [:node :parent-id])
        )
      )

      (is
        (=
          "n1(n1a(n1b))"
          (node/to-string t4)
        )
      )
    )
  )

  (testing "remove-node"
    (let
      [
        ; t1: example tree
        t1 (node/create data1)

        ; t2: remove n1
        n1 (node/get-node-by-name t1 "n1")
        [t2 info-t2] (node/remove-node n1)

        ; t3: remove n2a
        n2a (node/get-node-by-name t2 "n2a")
        [t3 info-t3] (node/remove-node n2a)

        ; t4: add n2c
        n2b (node/get-node-by-name t3 "n2b")
        [t4 info-t4] (node/add-node t3 n2b {:name "n2c" :id 7})

        ; t5: remove n2c
        n2c (node/get-node-by-name t4 "n2c")
        [t5 info-t5] (node/remove-node n2c)
      ]
      (is
        (=
          "n2(n2a n2b)"
          (node/to-string t2)
        )
      )

      (is
        (empty?
          (:changed-nodes info-t2)
        )
      )

      (is
        (=
          #{1 2 3}
          (set
            (map
              :id
              (:removed-nodes info-t2)
            )
          )
        )
      )

      (is
        (=
          "n2(n2b)"
          (node/to-string t3)
        )
      )

      (is
        (=
          '(4)
          (map
            :id
            (:changed-nodes info-t3)
          )
        )
      )

      (is
        (=
          #{5}
          (set
            (map
              :id
              (:removed-nodes info-t3)
            )
          )
        )
      )

      (is
        (=
          "n2(n2b(n2c))"
          (node/to-string t4)
        )
      )

      (is
        (=
          "n2(n2b)"
          (node/to-string t5)
        )
      )

    )
  )

  (testing "create"
    (let
      [
        t1 (node/create)
      ]
      (is
        (=
          ""
          (node/to-string t1)
        )
      )
    )
  )

  (testing "create from data"
    (let
      [
        t1 (node/create data1)
        n1a (node/get-node-by-name t1 "n1a")
      ]
      (is
        (=
          "n1(n1a n1b) n2(n2a n2b)"
          (node/to-string t1)
        )
      )
      (is
        (=
          1
          (get-in n1a [:node :parent-id])
        )
      )
    )
  )

  (testing "update-node"
    (let
      [
        t1 (node/create data1)

        n2a (node/get-node-by-name t1 "n2a")
        [t2 update-t2] (node/update-node n2a {:name "N2A" :color "green"})
      ]
      (is
        (=
          5
          (get-in
            (node/get-node-by-name t1 "n2a")
            [:node :id]
          )
        )
      )

      (is
        (=
          5
          (get-in
            (node/get-node-by-name t2 "N2A")
            [:node :id]
          )
        )
      )

      (is
        (=
          "green"
          (get-in
            (node/get-node-by-name t2 "N2A")
            [:node :color]
          )
        )
      )

      (is
        (=
          "N2A n2"
          (node/node-list-to-string (:changed-nodes update-t2))
        )
      )

      (is
        (=
          "n1(n1a n1b) n2(N2A n2b)"
          (node/to-string t2)
        )
      )
    )
  )

  (testing "get-node-by-name"
    (let
      [
        t1 (node/create data1)
        n1a (node/get-node-by-name t1 "n1a")
      ]
      (is
        (=
          "n1a"
          (get-in n1a [:node :name])
        )
      )

      (is
        (=
          "n1 [root]"
          (node/node-list-to-string (:parents n1a))
        )
      )
    )
  )
)
