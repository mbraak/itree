(ns itree.cljs.core
  (
    :require
    [reagent.core :as reagent :refer [atom]]
    [devcards.core :refer-macros [defcard]]
    [itree.cljc.tree :as tree]
    [itree.cljs.tree-component :refer [tree-component]]
  )
)

(enable-console-print!)

(defonce example-data
[{:children
  [{:name "Herrerasaurians", :id 2}
   {:children
    [{:name "Coelophysoids", :id 4}
     {:name "Ceratosaurians", :id 5}
     {:name "Spinosauroids", :id 6}
     {:name "Carnosaurians", :id 7}
     {:children
      [{:name "Tyrannosauroids", :id 9}
       {:name "Ornithomimosaurians", :id 10}
       {:name "Therizinosauroids", :id 11}
       {:name "Oviraptorosaurians", :id 12}
       {:name "Dromaeosaurids", :id 13}
       {:name "Troodontids", :id 14}
       {:name "Avialans", :id 15}]
      :name "Coelurosaurians"
      :id 8}]
    :name "Theropods"
    :id 3}
   {:children
    [{:name "Prosauropods", :id 17}
     {:children
      [{:name "Diplodocoids", :id 19}
       {:children
        [{:name "Brachiosaurids", :id 21} {:name "Titanosaurians", :id 22}]
        :name "Macronarians"
        :id 20}]
      :name "Sauropods"
      :id 18}]
    :name "Sauropodomorphs"
    :id 16}]
  :name "Saurischia"
  :id 1}
 {:children
  [{:name "Heterodontosaurids", :id 24}
   {:children
    [{:name "Ankylosaurians", :id 26} {:name "Stegosaurians", :id 27}]
    :name "Thyreophorans"
    :id 25}
   {:children [{:name "Hadrosaurids", :id 29}], :name "Ornithopods", :id 28}
   {:name "Pachycephalosaurians", :id 30}
   {:name "Ceratopsians", :id 31}]
  :name "Ornithischians"
  :id 23}]
)

(defonce example-tree
  (tree/create example-data)
)

(defcard tree
  (fn [data-atom owner]
    (reagent/as-element (tree-component data-atom))
  )
  (atom example-tree)
)

(defn on-js-reload []
  ;; optionally touch your app-state to force rerendering depending on
  ;; your application
  ;; (swap! app-state update-in [:__figwheel_counter] inc)
)
