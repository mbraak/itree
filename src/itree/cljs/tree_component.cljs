(ns itree.cljs.tree-component
  (:require
    [itree.cljc.node :as node]
    [itree.cljc.tree :as tree]
    [itree.cljs.util :refer [classes]]
  )
)

(declare tree-node)

(defn tree-folder [n tree-atom]
  (let
    [
      children (node/get-children n)

      ul-classes
      (classes
        {
          "banyan-common" true
          "banyan-tree" (:is-root n)
        }
      )
    ]
    [:ul {:class ul-classes}
      (for [child children]
        ^{:key (:id child)} [tree-node child tree-atom]
      )
    ]
  )
)

(defn toggle [id tree-atom]
  (reset!
    tree-atom
    (tree/toggle-node @tree-atom id)
  )
)

(defn select [id tree-atom]
  (reset!
    tree-atom
    (tree/select-node @tree-atom id)
  )
)

(defn tree-button [n]
  (let
    [
      is-open (:is-open n)

      button-classes
      (classes
        {
          "banyan-common" true
          "banyan-toggler" true
          "banyan-closed" (not is-open)
        }
      )

      button-char
      (if is-open "▼" "►")
    ]
    [:a
      {
        :href "#"
        :class button-classes
      }
      button-char
    ]
  )
)

(defn tree-title [n]
  (let
    [
      title-classes
      (classes
        {
          "banyan-common" true
          "banyan-title" true
          "banyan-title-folder" (node/has-children? n)
        }
      )
      random-number (int (* 100 (. js/Math random)))
    ]
    [:span
      {
        :class title-classes
      }
      (:name n)
      random-number
    ]
  )
)

(defn tree-node [n tree-atom]
  (let
    [
      id (:id n)
      is-folder (node/has-children? n)
      is-open-folder (and is-folder (:is-open n))
      children (:children get)
      is-selected (:is-selected n)

      li-classes
      (classes
        {
          "banyan-common" true
          "banyan-closed" (not is-open-folder)
          "banyan-folder" is-folder
          "banyan-selected" is-selected
        }
      )
    ]
    [:li {:key id :class li-classes}
      [:div
        {
          :class "banyan-element banyan-common"

          :on-click
          (fn [e]
            (.preventDefault e)

            (if
              (=
                (-> e .-target .-tagName)
                "A"
              )
              (toggle id tree-atom)
              (select id tree-atom)
            )
          )
        }
        [tree-title n]
        (when is-folder
          [tree-button n]
        )
      ]
      (when is-open-folder
        [tree-folder n tree-atom]
      )
    ]
  )
)

(defn tree-component [tree-atom]
  [tree-folder (:root @tree-atom) tree-atom]
)
