(ns itree.cljs.util
  (:require
    [clojure.string :refer [join]]
  )
)

(defn classes [m]
  (->>
    (filter
      (fn [[class must-include]]
        must-include
      )
      m
    )
    (map
      (fn [[class must-include]]
        class
      )
    )
    (join " ")
  )
)
