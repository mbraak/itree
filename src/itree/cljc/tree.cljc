(ns itree.cljc.tree
  (:require
    [clojure.string :refer [join]]
    [itree.cljc.node :as node]
    [clojure.pprint :refer [pprint]]
    [clojure.spec.alpha :as s]
  )
)

(s/def ::tree
  (s/keys :req-un [::root ::selected ::ids])
)

(defn- id-node-pair [node]
  [(:id node) node]
)

; todo: spec for id-node-pair

(defn- update-ids [ids updated-nodes deleted-ids]
  (let
    [
      updated-id-map
      (apply
        hash-map
        (mapcat id-node-pair updated-nodes)
      )
    ]
    (apply dissoc
      (merge ids updated-id-map)
      deleted-ids
    )
  )
)

(s/fdef update-ids
  :args (s/cat :ids map? :updated-nodes ::node/nodes :deleted-ids ::node/ids)
  :ret ::node/ids
)

(defn- update-tree [t new-root updated-nodes deleted-ids]
  (let
    [
      new-ids
      (update-ids
        (:ids t)
        updated-nodes
        deleted-ids
      )
    ]
    (assoc t
      :root new-root
      :ids new-ids
    )
  )
)

(s/fdef update-tree
  :args (s/cat :t ::tree :new-root ::node/node :updated-nodes ::node/nodes :deleted-ids ::node/ids)
  :ret ::tree
)

(defn- create-id-map [root]
  (let
    [
      pairs
      (mapcat
        #(list (:id %) %)
        (node/iterate-tree root)
      )
    ]
    (apply hash-map pairs)
  )
)

(s/fdef create-id-map
  :args (s/cat :root ::node/node)
  :ret map?
)

(defn create
  ([]
    {
      :root (node/create)
      :selected nil
      :ids (hash-map)
    }
  )

  ([data]
    (let
      [
        root (node/create data)
        ids (create-id-map root)
      ]
      {
        :root root
        :selected nil
        :ids ids
      }
    )
  )
)

(s/fdef create
  :args
  (s/or
    :create-empy empty?
    :create-with-data (s/cat :data ::node/node-data-list)
  )

  :ret ::tree
)

(defn to-string [t]
  (node/to-string (:root t))
)

(s/fdef to-string
  :args (s/cat :t ::tree)
  :ret string?
)

(defn get-children [t]
  (node/get-children (:root t))
)

(s/fdef get-children
  :args (s/cat :t ::tree)
  :ret ::node/nodes
)

(defn get-node-by-id [t id]
  (get (:ids t) id)
)

(s/fdef get-node-by-id
  :args (s/cat :t ::tree :id ::node/id)
  :ret ::node/node-or-nil
)

(defn get-parents [t node]
  (if (:is-root node)
    ; root
    (list)

    ; non-root
    (let
      [
        parent-id (:parent-id node)
        top-level? (nil? parent-id)

        parent
        (if top-level?
          nil
          (get-node-by-id t parent-id)
        )
      ]
      (if top-level?
        [(:root t)]
        (cons
          parent
          (get-parents t parent)
        )
      )
    )
  )
)

(s/fdef get-parents
  :args (s/cat :t ::tree :node ::node/node)
  :ret ::node/nodes
)

(defn- get-readonly-node [t node]
  {
    :node node
    :parents (get-parents t node)
  }
)

(s/fdef get-readonly-node
  :args (s/cat :t ::tree :node ::node/node)
  :ret ::node/readonly-node
)

(defn add-node
  ([t parent child]
    (let
      [
        root (:root t)
        readonly-parent (get-readonly-node t parent)
        parent-id (:id parent)

        [new-root update-info]
        (node/add-node root readonly-parent child)

        changed-nodes (:changed-nodes update-info)
        new-child (:new-child update-info)
      ]
      (update-tree
        t
        new-root
        (conj changed-nodes new-child)
        []
      )
    )
  )

  ([t child]
    (let
      [
        root (:root t)

        [new-root update-info] (node/add-node root child)

        new-child (:new-child update-info)
      ]
      (update-tree
        t
        new-root
        [new-child]
        []
      )
    )
  )
)

(s/fdef add-node
  :args
  (s/or
    :add-to-root (s/cat :t ::tree :child ::node/node)
    :add-to-parent (s/cat :t ::tree :parent ::node/node :child ::node/node)
  )

  :ret ::tree
)


(defn get-node-by-name [t name]
  (let
    [
      found-node
      (node/get-node-by-name (:root t) name)
    ]
    (if (nil? found-node)
      nil
      (:node found-node)
    )
  )
)

(s/fdef get-node-by-name
  :args (s/cat :t ::tree :name string?)
  :ret ::node/node-or-nil
)

(defn remove-node [t node]
  (let
    [
      readonly-node (get-readonly-node t node)

      [new-root affected-info]
      (node/remove-node readonly-node)
    ]
    (update-tree
      t
      new-root
      (:changed-nodes affected-info)
      (map
        :id
        (:removed-nodes affected-info)
      )
    )
  )
)

(s/fdef remove-node
  :args (s/cat :t ::tree :node ::node/node)
  :ret ::tree
)

(defn has-children? [t]
  (node/has-children? (:root t))
)

(s/fdef has-children?
  :args (s/cat :t ::tree)
  :ret boolean?
)

(defn is-node-open [t id]
  (let
    [
      n (get-node-by-id t id)
    ]
    (if (nil? n)
      false
      (:is-open n)
    )
  )
)

(s/fdef is-node-open
  :args (s/cat :t ::tree :id ::node/id)
  :ret boolean?
)

(defn update-node [t n attributes]
  (let
    [
      [new-root update-info]
      (node/update-node
        (get-readonly-node t n)
        attributes
      )
    ]
    (update-tree
      t
      new-root
      (:changed-nodes update-info)
      []
    )
  )
)

(s/fdef update-node
  :args (s/cat :t ::tree :n ::node/node :attributes map?)
  :ret ::tree
)

(defn open-node [t id]
  (let
    [
      n (get-node-by-id t id)
    ]
    (if (nil? n)
      t
      (update-node t n {:is-open true})
    )
  )
)

(s/fdef open-node
  :args (s/cat :t ::tree :id ::node/id)
  :ret ::tree
)

(defn close-node [t id]
  (let
    [
      n (get-node-by-id t id)
    ]
    (if (nil? n)
      t
      (update-node t n {:is-open false})
    )
  )
)

(s/fdef close-node
  :args (s/cat :t ::tree :id ::node/id)
  :ret ::tree
)

(defn toggle-node [t id]
  (let
    [
      n (get-node-by-id t id)
    ]
    (cond
      (nil? n) t
      (:is-open n) (close-node t id)
      :else (open-node t id)
    )
  )
)

(s/fdef toggle-node
  :args (s/cat :t ::tree :id ::node/id)
  :ret ::tree
)

(defn- deselect [t]
  (if (nil? (:selected t))
    t
    (let
      [
        n (get-node-by-id t (:selected t))
      ]
      (assoc
        (update-node t n {:is-selected false})
        :selected nil
      )
    )
  )
)

(s/fdef deselect
  :args (s/cat :t ::tree)
  :ret ::tree
)

(defn- update-node-by-id [t id attributes]
  (let
    [
      n (get-node-by-id t id)
    ]
    (if (nil? n)
      t
      (update-node t n attributes)
    )
  )
)

(s/fdef update-node-by-id
  :args (s/cat :t ::tree :id ::node/id :attributes map?)
  :ret ::tree
)

(defn select-node [t id]
  (->
    (deselect t)
    (assoc :selected id)
    (update-node-by-id id {:is-selected true})
  )
)

(s/fdef select-node
  :args (s/cat :t ::tree :id ::node/id)
  :ret ::tree
)
