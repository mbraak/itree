(ns itree.cljc.node
  (:require
      [clojure.string :refer [join]]
      [clojure.spec.alpha :as s]
  )
)

(s/def ::id
  (s/or :number int? :string string? :uuid uuid?)
)

(s/def ::ids
  (s/coll-of ::id)
)

(s/def ::name string?)

(def root
  {
    :name nil
    :id nil
    :is-root true
  }
)

(s/def ::root
  #(=
    (select-keys % [:name :id :is-root])
    root
  )
)

(s/def ::non-root-node
  (s/keys :req-un [::name ::id])
)

(s/def ::node
  (s/or :root ::root :non-root ::non-root-node)
)

(s/def ::nodes
  (s/coll-of ::node :distinct true)
)

(s/def ::node-data
  ::non-root-node
)

(s/def ::node-data-list
  (s/coll-of ::node-data :distinct true)
)

(s/def ::parents ::nodes)

(s/def ::readonly-node
  (s/keys :req-un [::node ::parents])
)

(s/def ::node-or-nil
  (s/or
    :node ::node
    :nil nil?
  )
)

(declare nodes-to-string)

(defn get-children [n]
  (get n :children [])
)

(s/fdef get-children
  :args (s/cat :n ::node)
  :ret ::nodes
)

(defn has-children? [n]
  (let
    [
      children (:children n)
    ]
    (not
      (or
        (nil? children)
        (empty? children)
      )
    )
  )
)

(s/fdef has-children?
  :args (s/cat :node ::node)
  :ret boolean?
)

(defn to-string [node]
  (let
    [
      children (get-children node)
      name (get node :name)
      root? (nil? name) ;; todo: use is-root ?
    ]
    (cond
      root?
      (nodes-to-string children)

      (empty? children)
      name

      :else
      (format
        "%s(%s)"
        name
        (nodes-to-string children)
      )
    )
  )
)

(s/fdef to-string
  :args (s/cat :node ::node)
  :ret string?
)

(defn- nodes-to-string [nodes]
  (join
    " "
    (map to-string nodes)
  )
)

(s/fdef nodes-to-string
  :args (s/cat :nodes ::nodes)
  :ret string?
)

(defn node-list-to-string [nodes]
  (join
    " "
    (map
      (fn [n]
        (if (:is-root n)
          "[root]"
          (:name n)
        )
      )
      nodes
    )
  )
)

(s/fdef node-list-to-string
  :args (s/cat :nodes ::nodes)
  :ret string?
)

(declare create-node-from-data)

(defn- create-nodes-from-data [parent-id children-data]
  (apply vector
    (map
      #(create-node-from-data parent-id %)
      children-data
    )
  )
)

(s/fdef create-nodes-from-data
  :args
    (s/cat
      :parent-id (s/or :id ::id :null nil?)
      :children-data ::node-data-list
    )

  :ret ::nodes
)

(defn- create-node-from-data [parent-id node-data]
  (let
    [
      node
      (assoc
        (dissoc node-data :children)
        :parent-id parent-id
      )

      node-id (:id node-data)

      children-data (get node-data :children [])
    ]
    (if (empty? children-data)
      node
      (assoc
        node
        :children
        (create-nodes-from-data node-id children-data)
      )
    )
  )
)

(s/fdef create-node-from-data
  :args
    (s/cat
      :parent-id (s/or :id ::id :null nil?)
      :node-data ::node-data
    )

  :ret ::nodes
)

(defn create
  ([]
    {
      :name nil
      :id nil
      :is-root true
    }
  )

  ([children-data]
    (assoc
      (create)
      :children
      (create-nodes-from-data nil children-data)
    )
  )
)

(s/fdef create
  :args
    (s/or
      :empty empty?
      :children-data (s/cat :children-data ::node-data-list)
    )

  :ret ::node
)


(defn- tree-seq-path
  "Like core's tree-seq, but returns [node parents] pairs.
  - returns lazy sequence
  - walks depth-first
  "
  [branch? children root]

  (letfn
    [
      (walk [path node]
        (lazy-seq
          (cons
            [node path]
            (when
              (branch? node)
              (mapcat
                (partial
                  walk
                  (cons node path)
                )
                (children node)
              )
            )
          )
        )
      )
    ]
    (walk (list) root)
  )
)

(s/fdef tree-seq-path
  :args
    (s/cat
      :branch? fn?
      :childen fn?
      :root ::node
    )

  :ret (s/* ::node)
)

(defn iterate-tree
  ([root]
    (iterate-tree root false)
  )

  ([root include-root?]
    (let
      [
        sequence
        (tree-seq has-children? get-children root)
      ]
      (if include-root?
        sequence
        (rest sequence)
      )
    )
  )
)

(s/fdef iterate-tree
  :args
    (s/or
      :def1 (s/cat :root ::node)
      :def2 (s/cat :root ::node :include-root? boolean?)
    )

  :ret (s/* ::node)
)

(defn iterate-tree-with-parents [root]
  "Iterate tree; return lazy sequence of readonly nodes"
  (letfn
    [
      (readonly-node [[node parents]]
        {
          :parents parents
          :node node
        }
      )
    ]
    (->>
      (tree-seq-path has-children? get-children root)
      (rest)
      (map readonly-node)
    )
  )
)

(s/fdef iterate-tree-with-parents
  :args (s/cat :root ::node)
  :ret (s/* ::readonly-node)
)

(defn get-node-by-name [root name]
  "Find node by name; return readonly node or nil"
  (letfn
    [
      (is-name [n]
        (= name (get-in n [:node :name]))
      )
    ]
    (->>
      (iterate-tree-with-parents root)
      (filter is-name)
      (first)
    )
  )
)

(s/fdef get-node-by-name
  :args (s/cat :node ::node :name ::name)

  :ret
  (s/or
    :null nil?
    :node ::readonly-node
  )
)

(defn- replace-child [node old-child new-child]
  (let
    [
      children (:children node)
      child-index (.indexOf children old-child)
      new-children (assoc children child-index new-child)
    ]
    (assoc
      node
      :children new-children
    )
  )
)

(s/fdef replace-child
  :args (s/cat :node ::node :old-child ::non-root-node :new-child ::non-root-node)
  :ret ::node
)

(defn- update-parents [old-child new-child parents]
  """
  Update parent of updated-node; also update the parents of the parent

  - 'old-child' is replaced by 'new-child'
  - 'parents' are the parents of the child; direct parent first
  - returns: [new root, affected]
  """
  (loop
    [
      old-child old-child
      new-child new-child
      parents parents
      new-parents (list)
    ]
    (if (empty? parents)
      ; return [new-root changed-nodes]
      (list
        (first new-parents)
        (rest new-parents)
      )

      ; continue loop
      (let
        [
          [parent & parents'] parents

          new-parent
          (replace-child parent old-child new-child)

          new-parents'
          (cons new-parent new-parents)
        ]
        (recur
          parent  ; old-child
          new-parent  ; new-child
          parents' ; parents
          new-parents'  ; new-parents
        )
      )
    )
  )
)

(s/fdef update-parents
  :args (s/cat :old-child ::non-root-node :new-child ::non-root-node :parents ::nodes)

  :ret (s/cat :root ::node :affected ::nodes)
)

(defn- add-child [parent new-child]
  (let
    [
      children (get-children parent)
      new-children (conj children new-child)
    ]
    (assoc
      parent
      :children new-children
    )
  )
)

(s/fdef add-child
  :args (s/cat :parent ::node :new-child ::node)
  :ret ::node
)

(defn add-node
  "
  Add node
  - return [new-root {:new-child :changed-nodes}]
  "
  ([root readonly-parent child]
    (let
      [
        parent (:node readonly-parent)
        parent-id (:id parent)
        parents (:parents readonly-parent)

        new-child
        (assoc child :parent-id parent-id)

        new-parent
        (add-child parent new-child)

        [new-root changed-nodes]
        (update-parents parent new-parent parents)
      ]
        (list
        new-root
        {
          :new-child new-child
          :changed-nodes (cons new-parent changed-nodes)
        }
      )
    )
  )

  ([root child]
    (let
      [
        new-root (add-child root child)
      ]
      (list
        new-root
        {
          :new-child child
          :changed-nodes []
        }
      )
    )
  )
)

(s/fdef add-node
  :args
  (s/or
    :input1 (s/cat :root ::node :readonly-parent ::readonly-node :child ::node-data)
    :input2 (s/cat :root ::node :child ::node-data)
  )

  :ret
  (s/cat
    :root ::node
    :info (s/keys :req-un [::new-child ::changed-nodes])
  )
)

(defn- remove-child [node child]
  (let
    [
      children (get-children node)
      new-children
      (apply vector
        (remove #{child} children)
      )
    ]
    (assoc
      node
      :children new-children
    )
  )
)

(s/fdef remove-child
  :args (s/cat :node ::node :child ::non-root-node)
  :ret ::node
)

(defn- remove-node-from-root [root child]
  (let
    [
      new-root
      (remove-child root child)

      removed-nodes
      (apply vector
        (iterate-tree child true)
      )
    ]
    (list
      new-root
      {
        :changed-nodes []
        :removed-nodes removed-nodes
      }
    )
  )
)

(s/fdef remove-node-from-root
  :args (s/cat :root ::root :child ::non-root-node)
  :ret ::node
)

(defn- remove-node-from-parent [parents child]
  (let
    [
      [parent & parents'] parents

      new-parent
      (remove-child parent child)

      [new-root changed-parents]
      (update-parents parent new-parent parents')

      removed-nodes
      (apply vector
        (iterate-tree child true)
      )
    ]
    (list
      new-root
      {
        :changed-nodes (cons new-parent changed-parents)
        :removed-nodes removed-nodes
      }
    )
  )
)

(s/fdef remove-node-from-parent
  :args (s/cat :parents ::nodes :child ::non-root-node)
  :ret ::node
)

(defn remove-node [readonly-child]
  """
  Remove node
  - return [new-root {:changed-nodes :removed-nodes}]
  """
  (let
    [
      child (:node readonly-child)
      parents (:parents readonly-child)

      parent (first parents)

      from-root? (:is-root parent)
    ]
    (if from-root?
      (remove-node-from-root parent child)
      (remove-node-from-parent parents child)
    )
  )
)

(s/fdef remove-node
  :args (s/cat :readonly-child ::readonly-node)

  :ret
  (s/cat
    :new-root ::node
    :info (s/keys :req-un [::changed-nodes ::removed-nodes])
  )
)

(defn update-node [readonly-node attributes]
  (let
    [
      node (:node readonly-node)
      parents (:parents readonly-node)
      new-node (merge node attributes)

      [new-root changed-parents]
      (update-parents node new-node parents)
    ]
    (list
      new-root
      {
        :changed-nodes (cons new-node changed-parents)
      }
    )
  )
)

(s/fdef update-node
  :args (s/cat :readonly-node ::readonly-node :attributes map?)

  :ret
  (s/cat
    :new-root ::node
    :info (s/keys :req-un [::changed-nodes])
  )
)
